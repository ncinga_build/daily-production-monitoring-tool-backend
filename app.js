const express = require('express');
const cors = require('cors');
const client = require('./config/es_connection');

const app = express();

const events = require('./routes/events');

// Port number
const port = 3000;

// CORS Middleware
app.use(cors());

app.use('/events',events);

// Index Route
app.get('/', (req,res) => {
    res.send('Invalid Endpoint');
});

// Start Server
app.listen(port, () => {
    console.log('Server started on port '+ port);
});