// Retrieve
var MongoClient = require('mongodb').MongoClient;

// Connect to the db
MongoClient.connect("mongodb://<server>:27017/<dbname>", function(err, db) {
    if(!err) {
        console.log("MongoDB connected!");
    }
});