var client = require('../config/es_connection');

module.exports.getFirstEvent = function(data, callback){

    var index = '*-' + data.factory + '-activity-elog-' + data.date;
    var line = '*-' + data.factory + '-' + data.line;
    var hsk = data.hsk;
    var at = data.at;
    
    client.search({

        index: index,
        body: {
            "size": 1,
            "sort": [
                {
                    "w1.dt": {
                        "order": "asc"
                    }
                }
            ], 
            "query": {
                "bool": {
                    "must": [
                        {
                        "wildcard":
                            { 
                                "a.sub.keyword": line
                            }
                        },
                        {
                            "term": 
                            {
                                "a.aux.hsk.keyword": hsk
                            }
                        },
                        {
                            "term":
                            {
                                "w2.at.keyword": at
                            }
                        }
                    ]
                }
            },
            "_source": ["w1.dt","w1.os"]
        }
        
    }, callback);

};

module.exports.getTimeEvent = function(data, callback){

    var index = data.factory + '-ops-time-' + data.date;
    var subject = data.line;
    
    client.search({

        index: "*ops-time*",
        body: {

            "size":0,
            "_source":
            {
                "excludes":[]
            },
            "aggs":
            {
                "2":
                {
                    "date_histogram":
                    {
                        "field":"time_tags.datetime",
                        "interval":"1m",
                        "time_zone":"Asia/Kolkata",
                        "min_doc_count":1
                    },
                    "aggs":
                    {
                        "1":
                        {
                            "sum":
                            {
                                "field":"points.attrib_val"
                            }
                        }
                    }
                }
            },
            "stored_fields":["*"],
            "script_fields":{},
            "docvalue_fields":
            [
                "time_tags.datetime",
                "time_tags.sessionDate"
            ],
            "query":
            {
                "bool":
                {
                    "must":
                    [
                        {
                            "match_all":{}
                        },
                        {
                            "match_phrase":
                            {
                                "_index":
                                {
                                    "query":index
                                }
                            }
                        },
                        {
                            "term": 
                            {
                                "subject":subject
                            }
                        }
                    ]
                }
            }
        }
        
    }, callback);

};
