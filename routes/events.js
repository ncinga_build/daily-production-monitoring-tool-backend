const express = require('express');
const router = express.Router();

const config = require('../config/es_connection');
const Event = require('../models/events');

// Get first event of a line
router.get('/get_first_event', (req, res, next) => {

    let newdata = {
        line:req.query.line,
        date:req.query.date,
        factory:req.query.factory,
        hsk:req.query.hsk,
        at:req.query.at
    };

    Event.getFirstEvent(newdata, (error, response) => {
        
        if (error){
            console.log("search error: "+error);
        }
        else {
            res.json(response);
        }

    });

});

// Get Time Event
router.get('/get_time_event', (req, res, next) => {

    let newdata = {
        date:req.query.date,
        factory:req.query.factory,
        line:req.query.line
    };

    Event.getTimeEvent( newdata, (error, response) => {
        
        if (error){
            console.log("search error: "+error);
        }
        else {
            res.json(response);
        }

    });

});

// Test routes
router.get('/test_route', (req, res, next) => {

    res.json({
        success:true, 
        msg:'Routes are working!'
    });

});


module.exports = router;

